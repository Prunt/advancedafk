package solutions.nuhvel.spigot.advancedafk.storage;

import org.bukkit.ChatColor;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import solutions.nuhvel.spigot.advancedafk.AdvancedAFK;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ConfigProvider {
    private AdvancedAFK plugin;
    private FileConfiguration config;
    private String name;

    public ConfigProvider(AdvancedAFK plugin, String name) {
        setMain(plugin);
        setName(name);
        loadConfig();
    }

    /* Configuration */

    public String getMessage(String path) {
        return ChatColor.translateAlternateColorCodes('&', getString(path));
    }

    public List<String> getMessageList(String path) {
        List<String> list = new ArrayList<>();
        for (String str : getConfig().getStringList(path)) {
            list.add(ChatColor.translateAlternateColorCodes('&', str));
        }
        return list;
    }

    public String getString(String path) {
        return getConfig().getString(path) == null ? "" : getConfig().getString(path);
    }

    public int getInt(String path) {
        return getConfig().getInt(path);
    }

    public double getDouble(String path) {
        return getConfig().getDouble(path);
    }

    public boolean isEnabled(String path) {
        return getConfig().getBoolean(path);
    }

    public List<String> getStringList(String path) {
        return getConfig().getStringList(path);
    }

    /* Methods */

    public boolean isNone(String... paths) {
        for (String path : paths) {
            if (!getMessage(path).equalsIgnoreCase(""))
                return false;
        }

        return true;
    }

    private FileConfiguration loadCustomConfig(String name) {
        File file = new File(getMain().getDataFolder(), name);

        if (!file.exists()) {
            file.getParentFile().mkdirs();
            getMain().saveResource(name, false);
        }

        FileConfiguration config = new YamlConfiguration();
        try {
            config.load(file);
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }

        return config;
    }

    private void loadConfig() {
        if (isDefault()) {
            getMain().saveDefaultConfig();
            getMain().reloadConfig();
            setConfig(getMain().getConfig());
        } else {
            setConfig(loadCustomConfig(getName()));
        }
    }

    private boolean isDefault() {
        return getName().equals("config.yml");
    }

    /* Getters & setters */

    private AdvancedAFK getMain() {
        return plugin;
    }

    private void setMain(AdvancedAFK advancedAFK) {
        this.plugin = advancedAFK;
    }

    public FileConfiguration getConfig() {
        return config;
    }

    public void setConfig(FileConfiguration config) {
        this.config = config;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
