package solutions.nuhvel.spigot.advancedafk.listeners;

import org.bukkit.Bukkit;
import org.bukkit.command.PluginCommand;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.player.*;
import solutions.nuhvel.spigot.advancedafk.AdvancedAFK;
import solutions.nuhvel.spigot.advancedafk.utils.AFKHandler;
import solutions.nuhvel.spigot.advancedafk.utils.AFKPlayer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AFKListener implements Listener {
    private final AdvancedAFK plugin;

    public AFKListener(AdvancedAFK plugin) {
        this.plugin = plugin;
    }

    private AdvancedAFK getMain() {
        return this.plugin;
    }

    @EventHandler
    public void onCommandPreprocess(PlayerCommandPreprocessEvent e) {
        String command = e.getMessage();
        Player p = e.getPlayer();
        AFKPlayer ap = AFKHandler.getAFKPlayer(getMain(), p);

        // World check
        if (getMain().getUtils().isDisabledWorld(p.getWorld().getName()))
            return;

        ConfigurationSection section = getMain().getSettings().getConfig().getConfigurationSection("commands");
        if (section == null) return;

        /* Old way of handling aliases */
        // Loop through command list
        for (String cmd : section.getKeys(false)) {
            // Loop through alias list
            for (String alias : getMain().getSettings().getStringList("commands." + cmd + ".aliases")) {
                // If the message matches or starts with the alias
                // (+ space to not catch other commands)
                if (command.startsWith("/" + alias + " ") || command.equalsIgnoreCase("/" + alias)) {
                    List<String> argList = new ArrayList<>(
                            Arrays.asList(command.substring(alias.length() + 1).split(" ")));
                    // Remove empty strings caused by double spaces and such
                    argList.removeAll(Arrays.asList("", null));
                    String[] arguments = argList.toArray(new String[0]);

                    PluginCommand pluginCommand = plugin.getCommand(cmd);
                    if (pluginCommand == null) continue;

                    pluginCommand.execute(p, alias, arguments);
                    e.setCancelled(true);
                    return;
                }
            }
        }

        if (e.getMessage().toLowerCase().startsWith("/afk"))
            return;

        if (AdvancedAFK.DEBUG)
            System.out.println("onCommandPreprocess: " + p.getName());

        // If this listener is enabled
        if (getMain().getSettings().isEnabled("listeners.commands"))
            // Removes player from AFK
            ap.setAFK(false);
    }

    @EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent e) {
        Player p = e.getPlayer();

        // World check
        if (getMain().getUtils().isDisabledWorld(p.getWorld().getName()))
            return;

        if (AdvancedAFK.DEBUG)
            System.out.println("onPlayerChat: " + p.getName());

        // If this listener is enabled
        if (getMain().getSettings().isEnabled("listeners.chat")) {
            // Removes player from AFK
            Bukkit.getScheduler().runTask(getMain(), () -> {
                AFKPlayer ap = AFKHandler.getAFKPlayer(getMain(), p);
                ap.setAFK(false);
            });
        }
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e) {
        Player p = e.getPlayer();
        AFKPlayer ap = AFKHandler.getAFKPlayer(getMain(), p);

        // World check
        if (getMain().getUtils().isDisabledWorld(p.getWorld().getName()))
            return;

        // If move protection is enabled and player doesn't have bypass permissions and
        // player is in AFK mode
        if (getMain().getSettings().isEnabled("protection.move")
                && !e.getPlayer().hasPermission("advancedafk.bypass.protection.move") && ap.isAFK()) {
            // Cancels the movement
            e.setCancelled(true);

            // Sends warning message
            getMain().getUtils().sendMessage(p, true, p, "protection.move");

            return;
        }

        if (AdvancedAFK.DEBUG)
            System.out.println("onPlayerMove: " + p.getName() + ", " + p.getFlySpeed() + ", " + p.getWalkSpeed());

        // If this listener is enabled
        if (getMain().getSettings().isEnabled("listeners.move"))
            // Removes player from AFK
            ap.setAFK(false);
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        AFKPlayer ap = AFKHandler.getAFKPlayer(getMain(), e.getPlayer());
        ap.resetTime();
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        Player p = e.getPlayer();
        AFKPlayer ap = AFKHandler.getAFKPlayer(getMain(), p);

        ap.setAFK(false, true);
        AFKHandler.removeAFKPlayer(p);
    }

    @EventHandler
    public void onPlayerInteractEntity(PlayerInteractEntityEvent e) {
        Player p = e.getPlayer();
        AFKPlayer ap = AFKHandler.getAFKPlayer(getMain(), p);

        // World check
        if (getMain().getUtils().isDisabledWorld(p.getWorld().getName()))
            return;

        if (AdvancedAFK.DEBUG)
            System.out.println("onPlayerInteractEntity: " + p.getName());

        // If this listener is enabled
        if (getMain().getSettings().isEnabled("listeners.interact.entity"))
            // Removes player from AFK
            ap.setAFK(false);
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        AFKPlayer ap = AFKHandler.getAFKPlayer(getMain(), p);

        // World check
        if (getMain().getUtils().isDisabledWorld(p.getWorld().getName()))
            return;

        if (AdvancedAFK.DEBUG)
            System.out.println("onPlayerInteract: " + p.getName());

        // Handle block interactions separately
        if (e.getAction() == Action.LEFT_CLICK_BLOCK) {
            // If this listener is enabled
            if (getMain().getSettings().isEnabled("listeners.block.left-click"))
                // Removes player from AFK
                ap.setAFK(false);

            return;
        }

        // If this listener is enabled
        if (getMain().getSettings().isEnabled("listeners.interact.anything"))
            // Removes player from AFK
            ap.setAFK(false);
    }

    @EventHandler
    public void onPlayerBedEnter(PlayerBedEnterEvent e) {
        Player p = e.getPlayer();
        AFKPlayer ap = AFKHandler.getAFKPlayer(getMain(), p);

        // World check
        if (getMain().getUtils().isDisabledWorld(p.getWorld().getName()))
            return;

        if (AdvancedAFK.DEBUG)
            System.out.println("onPlayerBedEnter: " + p.getName());

        // If this listener is enabled
        if (getMain().getSettings().isEnabled("listeners.bed-enter"))
            // Removes player from AFK
            ap.setAFK(false);
    }

    @EventHandler
    public void onPlayerChangedWorld(PlayerChangedWorldEvent e) {
        Player p = e.getPlayer();
        AFKPlayer ap = AFKHandler.getAFKPlayer(getMain(), p);

        // World check
        if (getMain().getUtils().isDisabledWorld(p.getWorld().getName()))
            return;

        if (AdvancedAFK.DEBUG)
            System.out.println("onPlayerChangedWorld: " + p.getName());

        // If this listener is enabled
        if (getMain().getSettings().isEnabled("listeners.world-change"))
            // Removes player from AFK
            ap.setAFK(false);
    }

    @EventHandler
    public void onPlayerEditBook(PlayerEditBookEvent e) {
        Player p = e.getPlayer();
        AFKPlayer ap = AFKHandler.getAFKPlayer(getMain(), p);

        // World check
        if (getMain().getUtils().isDisabledWorld(p.getWorld().getName()))
            return;

        if (AdvancedAFK.DEBUG)
            System.out.println("onPlayerEditBook: " + p.getName());

        // If this listener is enabled
        if (getMain().getSettings().isEnabled("listeners.book-edit"))
            // Removes player from AFK
            ap.setAFK(false);
    }

    @EventHandler
    public void onPlayerDropItem(PlayerDropItemEvent e) {
        Player p = e.getPlayer();
        AFKPlayer ap = AFKHandler.getAFKPlayer(getMain(), p);

        // World check
        if (getMain().getUtils().isDisabledWorld(p.getWorld().getName()))
            return;

        if (AdvancedAFK.DEBUG)
            System.out.println("onPlayerDropItem: " + p.getName());

        // If this listener is enabled
        if (getMain().getSettings().isEnabled("listeners.item.drop"))
            // Removes player from AFK
            ap.setAFK(false);
    }

    @EventHandler
    public void onPlayerPickupItem(EntityPickupItemEvent e) {
        if (!(e.getEntity() instanceof Player p))
            return;

        AFKPlayer ap = AFKHandler.getAFKPlayer(getMain(), p);

        // World check
        if (getMain().getUtils().isDisabledWorld(p.getWorld().getName()))
            return;

        if (AdvancedAFK.DEBUG)
            System.out.println("onPlayerPickupItem: " + p.getName());

        // If this listener is enabled
        if (getMain().getSettings().isEnabled("listeners.item.pickup"))
            // Removes player from AFK
            ap.setAFK(false);
    }

    @EventHandler
    public void onPlayerItemBreak(PlayerItemBreakEvent e) {
        Player p = e.getPlayer();
        AFKPlayer ap = AFKHandler.getAFKPlayer(getMain(), p);

        // World check
        if (getMain().getUtils().isDisabledWorld(p.getWorld().getName()))
            return;

        if (AdvancedAFK.DEBUG)
            System.out.println("onPlayerItemBreak: " + p.getName());

        // If this listener is enabled
        if (getMain().getSettings().isEnabled("listeners.item.break"))
            // Removes player from AFK
            ap.setAFK(false);
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e) {
        Player p = e.getPlayer();
        AFKPlayer ap = AFKHandler.getAFKPlayer(getMain(), p);

        // World check
        if (getMain().getUtils().isDisabledWorld(p.getWorld().getName()))
            return;

        if (AdvancedAFK.DEBUG)
            System.out.println("onBlockPlace: " + p.getName());

        // If this listener is enabled
        if (getMain().getSettings().isEnabled("listeners.block.place"))
            // Removes player from AFK
            ap.setAFK(false);
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        Player p = e.getPlayer();
        AFKPlayer ap = AFKHandler.getAFKPlayer(getMain(), p);

        // World check
        if (getMain().getUtils().isDisabledWorld(p.getWorld().getName()))
            return;

        if (AdvancedAFK.DEBUG)
            System.out.println("onBlockBreak: " + p.getName());

        // If this listener is enabled
        if (getMain().getSettings().isEnabled("listeners.block.break"))
            // Removes player from AFK
            ap.setAFK(false);
    }

    @EventHandler
    public void onPlayerShearEntity(PlayerShearEntityEvent e) {
        Player p = e.getPlayer();
        AFKPlayer ap = AFKHandler.getAFKPlayer(getMain(), p);

        // World check
        if (getMain().getUtils().isDisabledWorld(p.getWorld().getName()))
            return;

        if (AdvancedAFK.DEBUG)
            System.out.println("onPlayerShearEntity: " + p.getName());

        // If this listener is enabled
        if (getMain().getSettings().isEnabled("listeners.shear"))
            // Removes player from AFK
            ap.setAFK(false);
    }

    @EventHandler
    public void onPlayerToggleFlight(PlayerToggleFlightEvent e) {
        Player p = e.getPlayer();
        AFKPlayer ap = AFKHandler.getAFKPlayer(getMain(), p);

        // World check
        if (getMain().getUtils().isDisabledWorld(p.getWorld().getName()))
            return;

        if (AdvancedAFK.DEBUG)
            System.out.println("onPlayerToggleFlight: " + p.getName());

        // Block toggling flight when in AFK mode and move protection is enabled
        if (ap.isAFK() && getMain().getSettings().isEnabled("misc.protection.moving")
                && !p.hasPermission("advancedafk.bypass.protection.moving")) {
            e.setCancelled(true);
            return;
        }

        // If this listener is enabled
        if (getMain().getSettings().isEnabled("listeners.toggle.flight"))
            // Removes player from AFK
            ap.setAFK(false);
    }

    @EventHandler
    public void onPlayerToggleSprint(PlayerToggleSprintEvent e) {
        Player p = e.getPlayer();
        AFKPlayer ap = AFKHandler.getAFKPlayer(getMain(), p);

        // World check
        if (getMain().getUtils().isDisabledWorld(p.getWorld().getName()))
            return;

        if (AdvancedAFK.DEBUG)
            System.out.println("onPlayerToggleSprint: " + p.getName());

        // If this listener is enabled
        if (getMain().getSettings().isEnabled("listeners.toggle.sprint"))
            // Removes player from AFK
            ap.setAFK(false);
    }

    @EventHandler
    public void onPlayerToggleSneak(PlayerToggleSneakEvent e) {
        Player p = e.getPlayer();
        AFKPlayer ap = AFKHandler.getAFKPlayer(getMain(), p);

        // World check
        if (getMain().getUtils().isDisabledWorld(p.getWorld().getName()))
            return;

        if (AdvancedAFK.DEBUG)
            System.out.println("onPlayerToggleSneak: " + p.getName());

        // If this listener is enabled
        if (getMain().getSettings().isEnabled("listeners.toggle.sneak"))
            // Removes player from AFK
            ap.setAFK(false);
    }

    @EventHandler
    public void onPlayerUnleashEntity(PlayerUnleashEntityEvent e) {
        Player p = e.getPlayer();
        AFKPlayer ap = AFKHandler.getAFKPlayer(getMain(), p);

        // World check
        if (getMain().getUtils().isDisabledWorld(p.getWorld().getName()))
            return;

        if (AdvancedAFK.DEBUG)
            System.out.println("onPlayerUnleashEntity: " + p.getName());

        // If this listener is enabled
        if (getMain().getSettings().isEnabled("listeners.unleash"))
            // Removes player from AFK
            ap.setAFK(false);
    }

    @EventHandler
    public void onPlayerBucketFill(PlayerBucketFillEvent e) {
        Player p = e.getPlayer();
        AFKPlayer ap = AFKHandler.getAFKPlayer(getMain(), p);

        // World check
        if (getMain().getUtils().isDisabledWorld(p.getWorld().getName()))
            return;

        if (AdvancedAFK.DEBUG)
            System.out.println("onPlayerBucketFill: " + p.getName());

        // If this listener is enabled
        if (getMain().getSettings().isEnabled("listeners.bucket.fill"))
            // Removes player from AFK
            ap.setAFK(false);
    }

    @EventHandler
    public void onPlayerBucketEmpty(PlayerBucketEmptyEvent e) {
        Player p = e.getPlayer();
        AFKPlayer ap = AFKHandler.getAFKPlayer(getMain(), p);

        // World check
        if (getMain().getUtils().isDisabledWorld(p.getWorld().getName()))
            return;

        if (AdvancedAFK.DEBUG)
            System.out.println("onPlayerBucketEmpty: " + p.getName());

        // If this listener is enabled
        if (getMain().getSettings().isEnabled("listeners.bucket.empty"))
            // Removes player from AFK
            ap.setAFK(false);
    }

    @EventHandler
    public void onPlayerExpChange(PlayerExpChangeEvent e) {
        Player p = e.getPlayer();
        AFKPlayer ap = AFKHandler.getAFKPlayer(getMain(), p);

        // World check
        if (getMain().getUtils().isDisabledWorld(p.getWorld().getName()))
            return;

        if (AdvancedAFK.DEBUG)
            System.out.println("onPlayerExpChange: " + p.getName());

        // If this listener is enabled
        if (getMain().getSettings().isEnabled("listeners.xp"))
            // Removes player from AFK
            ap.setAFK(false);
    }
}
