package solutions.nuhvel.spigot.advancedafk;

import org.bukkit.command.CommandExecutor;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import solutions.nuhvel.spigot.advancedafk.commands.AFK;
import solutions.nuhvel.spigot.advancedafk.commands.CheckAFK;
import solutions.nuhvel.spigot.advancedafk.commands.MainCommand;
import solutions.nuhvel.spigot.advancedafk.listeners.AFKListener;
import solutions.nuhvel.spigot.advancedafk.storage.ConfigProvider;
import solutions.nuhvel.spigot.advancedafk.tasks.AFKCheck;
import solutions.nuhvel.spigot.advancedafk.utils.AFKExpansion;
import solutions.nuhvel.spigot.advancedafk.utils.AFKHandler;
import solutions.nuhvel.spigot.advancedafk.utils.Utils;

import java.util.Map;
import java.util.Map.Entry;

public class AdvancedAFK extends JavaPlugin {
    public static boolean DEBUG = false;

    private ConfigProvider config;
    private ConfigProvider messages;
    private Utils utils;

    @Override
    public void onEnable() {
        registerBungeecordChannel();
        createUtils();
        loadConfig();
        registerListeners();
    }

    @Override
    public void onDisable() {
        unloadPlayers();
    }

    private void unloadPlayers() {
        for (Player p : getServer().getOnlinePlayers()) {
            AFKHandler.getAFKPlayer(this, p).setAFK(false, true);
            AFKHandler.removeAFKPlayer(p);
        }
    }

    private void registerBungeecordChannel() {
        this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
    }

    private void createUtils() {
        setUtils(new Utils(this));
    }

    /**
     * Load settings and messages from config files
     */
    public void loadConfig() {
        this.config = new ConfigProvider(this, "config.yml");
        this.messages = new ConfigProvider(this, "messages.yml");

        // Reload command messages, aliases etc. as well
        registerCommands();

        // AFK check loop
        int interval = getSettings().getInt("auto.interval") * 20;
        getServer().getScheduler().cancelTasks(this);
        getServer().getScheduler().scheduleSyncRepeatingTask(this, new AFKCheck(this), interval, interval);
    }

    /**
     * Register plugin commands
     */
    private void registerCommands() {
        // Register commands
        for (Entry<String, Map<String, Object>> entry : getDescription().getCommands().entrySet()) {
            String name = entry.getKey();

            PluginCommand cmd = getCommand(name);
            if (cmd == null) continue;

            cmd.setExecutor(getExecutor(name));
            cmd.setPermissionMessage(getMessages().getMessage("no-permission"));
            cmd.setDescription(getSettings().getMessage("commands." + name + ".description"));
            cmd.setUsage(getSettings().getMessage("commands." + name + ".usage"));
        }
    }

    /**
     * Register event listeners
     */
    private void registerListeners() {
        getServer().getPluginManager().registerEvents(new AFKListener(this), this);

        // Register PlaceholderAPI expansion
        if (Utils.isInstalled("PlaceholderAPI")) {
            new AFKExpansion(this).register();
        }
    }

    /**
     * @return Settings provider
     */
    public ConfigProvider getSettings() {
        return this.config;
    }

    /**
     * @return Messages provider
     */
    public ConfigProvider getMessages() {
        return this.messages;
    }

    public Utils getUtils() {
        return utils;
    }

    public void setUtils(Utils utils) {
        this.utils = utils;
    }

    private CommandExecutor getExecutor(String name) {
        return switch (name) {
            case "afk" -> new AFK(this);
            case "checkafk" -> new CheckAFK(this);
            case "advancedafk" -> new MainCommand(this);
            default -> null;
        };
    }
}
