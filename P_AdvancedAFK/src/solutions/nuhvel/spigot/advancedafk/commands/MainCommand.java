package solutions.nuhvel.spigot.advancedafk.commands;

import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import solutions.nuhvel.spigot.advancedafk.AdvancedAFK;

public class MainCommand implements CommandExecutor {
    private final AdvancedAFK plugin;

    public MainCommand(AdvancedAFK plugin) {
        this.plugin = plugin;
    }

    private AdvancedAFK getMain() {
        return this.plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length == 1 && args[0].equalsIgnoreCase("set") && sender instanceof Player p
                && !getMain().getUtils().isDisabledWorld(((Player) sender).getWorld().getName())) {
            World world = p.getLocation().getWorld();
            if (world == null) throw new IllegalArgumentException("World doesn't exist");

            // Save location to config
            getMain().getConfig().set("auto.teleport.location.x", p.getLocation().getX());
            getMain().getConfig().set("auto.teleport.location.y", p.getLocation().getY());
            getMain().getConfig().set("auto.teleport.location.z", p.getLocation().getZ());
            getMain().getConfig().set("auto.teleport.location.yaw", p.getLocation().getYaw());
            getMain().getConfig().set("auto.teleport.location.pitch", p.getLocation().getPitch());
            getMain().getConfig().set("auto.teleport.location.world", world.getName());
            getMain().saveConfig();

            // Sends confirmation message
            getMain().getUtils().sendMessage(p, true, p, "teleport.set");

            return true;
        }

        getMain().loadConfig();
        getMain().getUtils().sendMessage(sender, true, null, "reloaded");

        return true;
    }
}
