package solutions.nuhvel.spigot.advancedafk.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import solutions.nuhvel.spigot.advancedafk.AdvancedAFK;
import solutions.nuhvel.spigot.advancedafk.utils.AFKHandler;
import solutions.nuhvel.spigot.advancedafk.utils.AFKPlayer;
import solutions.nuhvel.spigot.advancedafk.utils.Utils;

import java.time.Instant;
import java.util.Arrays;

public class AFK implements CommandExecutor {
    private final AdvancedAFK plugin;

    public AFK(AdvancedAFK plugin) {
        this.plugin = plugin;
    }

    private AdvancedAFK getMain() {
        return this.plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length == 0) {
            afkNoReason(sender);
            return true;
        }

        Player p = Bukkit.getPlayer(args[0]);

        if (sender.hasPermission("advancedafk.commands.afk.others") && p != null && p.isOnline()) {
            if (args.length == 1) {
                afkNoReason(p);
                return true;
            }

            afkWithReason(p, String.join(" ", Arrays.asList(args).subList(1, args.length)), true);
        } else {
            // Console can't be AFK, so first parameter must've been a player
            if (!(sender instanceof Player)) {
                Utils.sendMessage(sender, plugin.getUtils().getMessage(true, "not-found").replaceAll("%player%", args[0]),
                        null);
                return true;
            }

            afkWithReason((Player) sender, String.join(" ", args), false);
        }

        return true;
    }

    private void afkWithReason(Player p, String reason, boolean other) {
        // World check
        if (getMain().getUtils().isDisabledWorld(p.getWorld().getName()))
            return;

        AFKPlayer ap = AFKHandler.getAFKPlayer(getMain(), p);

        if (!other && cannotUseCommand(ap))
            return;

        if (p.hasPermission("advancedafk.allow-text-formatting"))
            reason = ChatColor.translateAlternateColorCodes('&', reason);

        // Reverse AFK mode
        ap.setAFK(!ap.isAFK(), reason);
    }

    private void afkNoReason(CommandSender sender) {
        if (!(sender instanceof Player p)) {
            getMain().getUtils().sendMessage(sender, true, null, "no-console");
            return;
        }

        AFKPlayer ap = AFKHandler.getAFKPlayer(getMain(), p);

        // World check
        if (getMain().getUtils().isDisabledWorld(p.getWorld().getName()))
            return;

        if (cannotUseCommand(ap))
            return;

        // Reverse AFK mode
        ap.setAFK(!ap.isAFK());
    }

    private boolean cannotUseCommand(AFKPlayer ap) {
        int timeLeft = getCooldown(ap);
        Player p = ap.getPlayer();

        if (timeLeft != 0 && getMain().getSettings().isEnabled("general.anti-spam.enabled")
                && !p.hasPermission("advancedafk.bypass.anti-spam.cooldown")) {
            Utils.sendMessage(p, plugin.getUtils().getMessage(true, "protection.spam").replaceAll("%seconds%",
                    String.valueOf(timeLeft)), p);

            return true;
        }

        ap.setLastUsedCmd(Instant.now().getEpochSecond());

        return false;
    }

    private int getCooldown(AFKPlayer ap) {
        long sinceLastUsed = Instant.now().getEpochSecond() - ap.getLastUsedCmd();
        long cooldown = getMain().getSettings().getInt("general.anti-spam.cooldown");
        int timeLeft = (int) (cooldown - sinceLastUsed);

        if (sinceLastUsed < cooldown)
            return timeLeft;

        return 0;
    }
}
