package solutions.nuhvel.spigot.advancedafk.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import solutions.nuhvel.spigot.advancedafk.AdvancedAFK;
import solutions.nuhvel.spigot.advancedafk.utils.AFKHandler;
import solutions.nuhvel.spigot.advancedafk.utils.AFKPlayer;
import solutions.nuhvel.spigot.advancedafk.utils.Utils;

public class CheckAFK implements CommandExecutor {
    private final AdvancedAFK plugin;

    public CheckAFK(AdvancedAFK plugin) {
        this.plugin = plugin;
    }

    private AdvancedAFK getMain() {
        return this.plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length != 1)
            return false;

        String pl = args[0];
        Player p = Bukkit.getPlayer(pl);

        if (p == null || !p.isOnline()) {
            Utils.sendMessage(sender, getMain().getUtils().getMessage(true, "not-found").replaceAll("%player%", pl), p);
            return true;
        }

        AFKPlayer ap = AFKHandler.getAFKPlayer(getMain(), p);

        String msg = getMain().getUtils().getMessage(true, "checkafk.first-line");
        msg = msg.replaceAll("%player%", p.getName()).replaceAll("%displayname%", p.getDisplayName());

        Utils.sendMessage(sender, msg, p);

        for (String message : getMain().getMessages().getMessageList("checkafk.lines")) {
            String status = getMain().getMessages().getMessage("checkafk.convert." + ap.isAFK());
            String duration = getMinutesSeconds(ap.getInactiveSecs());

            message = message.replaceAll("%status%", status).replaceAll("%duration%", duration)
                    .replaceAll("%player%", p.getName()).replaceAll("%displayname%", p.getDisplayName());

            Utils.sendMessage(sender, message, p);
        }

        return true;
    }

    private String getMinutesSeconds(int totalSecs) {
        int minutes = totalSecs / 60;
        int seconds = totalSecs % 60;

        String result = "";

        if (minutes > 0) {
            result += minutes + getMain().getMessages().getMessage("checkafk.convert.minutes");

            if (seconds > 0)
                result += ", ";
        }

        if (seconds > 0)
            result += seconds + getMain().getMessages().getMessage("checkafk.convert.seconds");

        return result.isEmpty() ? "-" : result;
    }
}
