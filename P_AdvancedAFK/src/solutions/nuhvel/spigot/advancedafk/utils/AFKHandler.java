package solutions.nuhvel.spigot.advancedafk.utils;

import org.bukkit.entity.Player;
import solutions.nuhvel.spigot.advancedafk.AdvancedAFK;

import java.util.HashMap;

public class AFKHandler {
    private static final HashMap<Player, AFKPlayer> playerCache = new HashMap<>();

    public static AFKPlayer getAFKPlayer(AdvancedAFK plugin, Player p) {
        if (playerCache.containsKey(p))
            return playerCache.get(p);

        AFKPlayer afkPlayer = new AFKPlayer(plugin, p);

        playerCache.put(p, afkPlayer);
        return afkPlayer;
    }

    public static void removeAFKPlayer(Player p) {
        playerCache.remove(p);
    }
}
