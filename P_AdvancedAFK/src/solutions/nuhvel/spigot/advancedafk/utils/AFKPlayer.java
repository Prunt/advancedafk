package solutions.nuhvel.spigot.advancedafk.utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;
import solutions.nuhvel.spigot.advancedafk.AdvancedAFK;
import solutions.nuhvel.spigot.advancedafk.tasks.Countdown;
import solutions.nuhvel.spigot.advancedafk.tasks.Particles;
import solutions.nuhvel.spigot.advancedafk.tasks.Task;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class AFKPlayer {
    private final AdvancedAFK plugin;
    private final Player p;
    private int inactiveSecs = 0;
    private boolean status = false;

    private String oldPlayerListName;
    private Location previousLocation;
    private long lastUsedCmd = 0;
    private boolean wasFlyAllowed;
    private boolean wasFlyEnabled;

    private final HashMap<String, Task> tasks = new HashMap<>();

    public AFKPlayer(AdvancedAFK plugin, Player p) {
        this.plugin = plugin;
        this.p = p;
    }

    private AdvancedAFK getMain() {
        return this.plugin;
    }

    public Player getPlayer() {
        return p;
    }

    public void setAFK(boolean status) {
        setAFK(status, null);
    }

    public void setAFK(boolean status, String reason) {
        setAFK(status, false, reason);
    }

    public void setAFK(boolean status, boolean silent) {
        setAFK(status, silent, null);
    }

    public void setAFK(boolean status, boolean silent, String reason) {
        if (isAFK() == status) {
            // If we should remove the AFK mode but the player isn't in AFK mode
            if (!status) {
                abortTasks();
                resetTime();
            }

            return;
        }

        if (status) {
            addAFK(reason);
        } else {
            removeAFK(silent);
        }

        this.status = status;
    }

    public boolean isAFK() {
        return status;
    }

    public String getOldPlayerListName() {
        return oldPlayerListName;
    }

    public void setOldPlayerListName(String oldPlayerListName) {
        this.oldPlayerListName = oldPlayerListName;
    }

    public Task getTask(String type) {
        return tasks.get(type);
    }

    public void setTask(String type, Task task) {
        this.tasks.put(type, task);
    }

    public Location getPreviousLocation() {
        return previousLocation;
    }

    public void setPreviousLocation(Location previousLocation) {
        this.previousLocation = previousLocation;
    }

    public int getInactiveSecs() {
        return inactiveSecs;
    }

    public void setInactiveSecs(int inactiveSecs) {
        this.inactiveSecs = inactiveSecs;
    }

    public void incrementInactiveSecs() {
        setInactiveSecs(getInactiveSecs() + getMain().getSettings().getInt("auto.interval"));
    }

    public long getLastUsedCmd() {
        return lastUsedCmd;
    }

    public void setLastUsedCmd(long l) {
        this.lastUsedCmd = l;
    }

    public boolean wasFlyAllowed() {
        return wasFlyAllowed;
    }

    public void setWasFlyAllowed(boolean wasFlyAllowed) {
        this.wasFlyAllowed = wasFlyAllowed;
    }

    public boolean wasFlyEnabled() {
        return wasFlyEnabled;
    }

    public void setWasFlyEnabled(boolean wasFlyEnabled) {
        this.wasFlyEnabled = wasFlyEnabled;
    }

    public void kick() {
        if (AdvancedAFK.DEBUG)
            System.out.println("kick " + getPlayer().getName());

        // Gets the command sender
        CommandSender sender = Bukkit.getConsoleSender();
        if (!getMain().getSettings().isEnabled("auto.kick.as-console")) {
            sender = getPlayer();
        }

        // Loop through all commands and execute them
        for (String cmd : getMain().getSettings().getStringList("auto.kick.commands")) {
            Bukkit.dispatchCommand(sender,
                    ChatColor.translateAlternateColorCodes('&', cmd).replaceAll("%player%", getPlayer().getName())
                            .replaceAll("%displayname%", getPlayer().getDisplayName()));

            if (AdvancedAFK.DEBUG)
                System.out.println("kick.commands " + cmd);
        }
    }

    public void teleport() {
        if (AdvancedAFK.DEBUG)
            System.out.println("teleport " + getPlayer().getName());

        setPreviousLocation(getPlayer().getLocation());

        // Define the location parameters
        double x = getMain().getSettings().getDouble("auto.teleport.location.x");
        double y = getMain().getSettings().getDouble("auto.teleport.location.y");
        double z = getMain().getSettings().getDouble("auto.teleport.location.z");
        float yaw = (float) getMain().getSettings().getDouble("auto.teleport.location.yaw");
        float pitch = (float) getMain().getSettings().getDouble("auto.teleport.location.pitch");
        String worldStr = getMain().getSettings().getString("auto.teleport.location.world");

        World world = Bukkit.getWorld(worldStr);

        if (world == null) {
            getMain().getUtils().sendMessage(Bukkit.getConsoleSender(), true, getPlayer(), "teleport.error");
            return;
        }

        // Teleport the player
        getPlayer().teleport(new Location(world, x, y, z, yaw, pitch));

        getMain().getUtils().sendMessage(getPlayer(), true, getPlayer(), "teleport.teleported");
    }

    private void addAFK(String reason) {
        // When tablist name changing is enabled
        if (getMain().getSettings().isEnabled("prefix.tablist.enabled")) {
            // Saves old tablist name
            setOldPlayerListName(getPlayer().getPlayerListName());

            String prefix = getMain().getSettings().getMessage("prefix.tablist.prefix").replaceAll("%reason%", reason);

            // Adds prefix in front of the tablist name
            getPlayer().setPlayerListName(prefix + getPlayer().getPlayerListName());
        }

        // When scoreboard tag changing is enabled
        if (getMain().getSettings().isEnabled("prefix.playertag.enabled")) {
            Scoreboard tags = getPlayer().getScoreboard();

            Team tag = tags.getTeam(getPlayer().getName());
            if (tag == null)
                tag = tags.registerNewTeam(getPlayer().getName());

            String prefix = getMain().getSettings().getMessage("prefix.playertag.prefix").replaceAll("%reason%",
                    reason);

            tag.setPrefix(prefix);
            tag.addEntry(getPlayer().getName());

            for (Player pl : Bukkit.getOnlinePlayers()) {
                pl.setScoreboard(tags);
            }
        }

        // When ignore sleep mode is enabled
        if (getMain().getSettings().isEnabled("misc.ignore-sleep"))
            getPlayer().setSleepingIgnored(true);

        // If damage protection is enabled
        if (getMain().getSettings().isEnabled("misc.protection.damage")
                && !getPlayer().hasPermission("advancedafk.bypass.protection.damage"))
            // Makes player invulnerable
            getPlayer().setInvulnerable(true);

        // If moving protection is enabled
        if (getMain().getSettings().isEnabled("misc.protection.moving")
                && !getPlayer().hasPermission("advancedafk.bypass.protection.moving")) {
            setWasFlyAllowed(getPlayer().getAllowFlight());
            setWasFlyEnabled(getPlayer().isFlying());

            Location loc = getPlayer().getLocation().clone();
            getPlayer().teleport(new Location(loc.getWorld(), loc.getX(), loc.getY() + 0.1, loc.getZ(), loc.getYaw(),
                    loc.getPitch()));

            getPlayer().setAllowFlight(true);
            getPlayer().setFlying(true);
            getPlayer().setFlySpeed(0);

            getPlayer().teleport(loc);
        }

        // Broadcasts the message
        getMain().getUtils().broadcastAfkEnabled(p, reason);

        if (!getMain().getSettings().isEnabled("misc.particles.enabled")
                || Utils.isVanished(getPlayer()))
            return;

        // Initiate particles
        Particles task = new Particles(getMain(), getPlayer());
        task.setID(Bukkit.getScheduler().scheduleSyncRepeatingTask(getMain(), task, 0, 20));

        setTask("particle", task);
    }

    private void removeAFK(boolean silent) {
        if (AdvancedAFK.DEBUG)
            System.out.println(getPlayer().getName() + " AFK removed " + silent);

        // If player is in AFK mode
        if (isAFK()) {
            // When tablist tag changing is enabled
            if (getMain().getSettings().isEnabled("prefix.tablist.enabled")) {
                // Resets the playerlist name
                getPlayer().setPlayerListName(getOldPlayerListName());
            }

            if (!silent)
                // Broadcasts the message
                getMain().getUtils().broadcast(getPlayer(), "afk.disabled", getPlayer());

            // Teleport player back to previous location
            if (getMain().getSettings().isEnabled("auto.teleport.enabled") && getPreviousLocation() != null) {
                getPlayer().teleport(getPreviousLocation());
                setPreviousLocation(null);
            }

            // When ignore sleep mode is enabled
            if (getMain().getSettings().isEnabled("misc.ignore-sleep"))
                getPlayer().setSleepingIgnored(false);

            // When scoreboard tag changing is enabled
            if (getMain().getSettings().isEnabled("prefix.playertag.enabled")) {
                Scoreboard tags = getPlayer().getScoreboard();
                Team tag = tags.getTeam(getPlayer().getName());

                if (tag != null) {
                    tag.setPrefix("");
                    tag.removeEntry(getPlayer().getName());

                    for (Player pl : Bukkit.getOnlinePlayers()) {
                        pl.setScoreboard(tags);
                    }
                }
            }

            // If damage protection is enabled
            if (getMain().getSettings().isEnabled("misc.protection.damage")
                    && !getPlayer().hasPermission("advancedafk.bypass.protection.damage"))
                // Removes player's god mode
                getPlayer().setInvulnerable(false);

            // If moving protection is enabled
            if (getMain().getSettings().isEnabled("misc.protection.moving")
                    && !getPlayer().hasPermission("advancedafk.bypass.protection.moving")) {
                getPlayer().setFlySpeed(0.1F);
                getPlayer().setFlying(wasFlyEnabled());
                getPlayer().setAllowFlight(wasFlyAllowed());
            }
        }

        // Abort player's tasks
        abortTasks();

        // Resets the counter
        resetTime();
    }

    public void abortTasks() {
        String[] tasks = {"particle", "afk", "teleport", "kick"};

        for (String task : tasks) {
            if (getTask(task) != null) {
                Bukkit.getScheduler().cancelTask(getTask(task).getID());
                setTask(task, null);
            }
        }
    }

    public void resetTime() {
        setInactiveSecs(0);
    }

    public void countdown(String type) {
        if (AdvancedAFK.DEBUG)
            System.out.println(p.getName() + " " + type + " " + getInactiveSecs() + " vs " + getDelay(type) + " ("
                    + (getTask(type) != null) + ")");

        // Abort when player has been inactive for less than required for action
        // or the countdown has already been started
        if (getInactiveSecs() < getDelay(type) || getTask(type) != null)
            return;

        switch (type) {
            case "afk":
                // Don't put into AFK mode if the player is already in it
                if (isAFK())
                    return;
                break;
            case "teleport":
                // Don't teleport the player if they've been already teleported
                if (getPreviousLocation() != null)
                    return;
                break;
            case "kick":
                // Don't kick the player if the server isn't full when the setting is enabled
                if (getMain().getSettings().isEnabled("auto.kick.only-if-full") && !Utils.isFull())
                    return;
                break;
        }

        if (!getMain().getSettings().isEnabled("countdown." + type + ".enabled")) {
            execute(type);
            return;
        }

        // Initiate countdown
        Countdown task = new Countdown(getMain(), getPlayer(), type);
        task.setID(Bukkit.getScheduler().scheduleSyncRepeatingTask(getMain(), task, 0, 20));

        setTask(type, task);
    }

    public void execute(String type) {
        switch (type) {
            case "afk" -> setAFK(true);
            case "teleport" -> teleport();
            case "kick" -> kick();
        }
    }

    // Return auto delay seconds
    private int getDelay(String type) {
        ConfigurationSection section = getMain().getSettings().getConfig().getConfigurationSection("presets");
        Set<String> presets = section != null ? section.getKeys(false) : new HashSet<>();

        // Loop through the preset list
        for (String perm : presets) {
            if (p.hasPermission("advancedafk.preset." + perm))
                return getMain().getSettings().getInt("presets." + perm + "." + type);
        }

        // Get default auto delay seconds
        return getMain().getSettings().getInt("auto." + type + ".delay");
    }
}
