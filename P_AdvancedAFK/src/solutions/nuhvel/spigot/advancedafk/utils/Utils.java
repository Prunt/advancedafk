package solutions.nuhvel.spigot.advancedafk.utils;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.metadata.MetadataValue;
import solutions.nuhvel.spigot.advancedafk.AdvancedAFK;

public class Utils {
    /* --- Static methods --- */
    public static boolean isInstalled(String plugin) {
        return Bukkit.getPluginManager().getPlugin(plugin) != null;
    }

    public static boolean isFull() {
        return Bukkit.getOnlinePlayers().size() == Bukkit.getMaxPlayers();
    }

    // https://www.spigotmc.org/resources/supervanish-be-invisible.1331/
    // This code is supported by SuperVanish, PremiumVanish, VanishNoPacket
    // and a few more vanish plugins.
    public static boolean isVanished(Player player) {
        for (MetadataValue meta : player.getMetadata("vanished")) {
            if (meta.asBoolean())
                return true;
        }
        return false;
    }

    // Print error to console
    public static void log(String msg) {
        Bukkit.getLogger().severe(msg);
    }

    public static void sendMessage(CommandSender receiver, String msg, Player subject) {
        if (msg.equalsIgnoreCase(""))
            return;

        if (Bukkit.getPluginManager().getPlugin("PlaceholderAPI") != null && subject != null)
            msg = PlaceholderAPI.setPlaceholders(subject, msg);

        receiver.sendMessage(msg);
    }

    /* --- Non-static methods --- */
    private final AdvancedAFK plugin;

    public Utils(AdvancedAFK plugin) {
        this.plugin = plugin;
    }

    private AdvancedAFK getMain() {
        return this.plugin;
    }

    /**
     * @param name World name
     * @return Whether the plugin is disabled in the given world
     */
    public boolean isDisabledWorld(String name) {
        return getMain().getSettings().getStringList("general.disabled-worlds").contains(name);
    }

    /**
     * @param sender Player to send the message to
     * @param prefix Whether to include a prefix in the message
     * @param paths  Paths of messages to send to the player
     */
    public void sendMessage(CommandSender sender, boolean prefix, Player subject, String... paths) {
        sendMessage(sender, getMessage(prefix, paths), subject);
    }

    /**
     * @param prefix Whether to include a prefix in the message
     * @param paths  Paths of messages to send to the player
     */
    public String getMessage(boolean prefix, String... paths) {
        if (getMain().getMessages().isNone(paths))
            return "";

        StringBuilder msg = new StringBuilder(prefix ? getMain().getMessages().getMessage("prefix") : "");
        for (String path : paths)
            msg.append(getMain().getMessages().getMessage(path));

        return msg.toString();
    }

    // Broadcasts message from specified path
    public void broadcastAfkEnabled(Player p, String reason) {
        if (reason != null) {
            String message = getMain().getMessages().getMessage("afk.enabled-reason");

            broadcastMessage(p, message.replaceAll("%reason%", reason), null);
        } else {
            broadcastMessage(p, getMain().getMessages().getMessage("afk.enabled"), p);
        }
    }

    // Broadcasts message from specified path
    public void broadcast(Player p, String path, Player subject) {
        broadcastMessage(p, getMain().getMessages().getMessage(path), subject);
    }

    // Broadcasts specified message
    private void broadcastMessage(Player p, String msg, Player subject) {
        // Doesn't broadcast anything when player is vanished
        if (isVanished(p))
            return;

        // Doesn't broadcast anything when player is dead
        if (!getMain().getSettings().isEnabled("general.broadcast.if-dead") && p.isDead())
            return;

        // Prepares the message
        msg = msg.replaceAll("%player%", p.getName()).replaceAll("%displayname%", p.getDisplayName());

        if (Bukkit.getPluginManager().getPlugin("PlaceholderAPI") != null && subject != null)
            msg = PlaceholderAPI.setPlaceholders(subject, msg);

        // If we should broadcast all over Bungeecord instance
        if (getMain().getSettings().isEnabled("general.broadcast.bungee")) {
            ByteArrayDataOutput out = ByteStreams.newDataOutput();
            out.writeUTF("Message");
            out.writeUTF("ALL");
            out.writeUTF(msg);

            p.sendPluginMessage(getMain(), "BungeeCord", out.toByteArray());

            // If messages.broadcast option is true
        } else if (getMain().getSettings().isEnabled("general.broadcast.everyone")) {
            // Broadcasts afk message to everyone
            Bukkit.broadcastMessage(msg);

            // If the broadcast option is set to private
        } else if (getMain().getSettings().isEnabled("general.broadcast.player")) {
            // Sends the message only to the player
            p.sendMessage(msg);
        }
    }
}
