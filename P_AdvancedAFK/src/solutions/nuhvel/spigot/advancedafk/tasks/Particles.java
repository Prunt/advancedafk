package solutions.nuhvel.spigot.advancedafk.tasks;

import org.bukkit.Bukkit;
import org.bukkit.Particle;
import org.bukkit.entity.Player;
import solutions.nuhvel.spigot.advancedafk.AdvancedAFK;

import java.util.logging.Level;

public class Particles extends Task {
    public Particles(AdvancedAFK plugin, Player p) {
        setMain(plugin);
        setPlayer(p);
    }

    @Override
    public void run() {
        int count = getMain().getSettings().getInt("misc.particles.amount");

        // Get valid Particle
        Particle particle;
        String value = getMain().getSettings().getString("misc.particles.type");
        try {
            particle = Particle.valueOf(value);
        } catch (IllegalArgumentException e) {
            Bukkit.getLogger().log(Level.WARNING,
                    "Couldn't find a Particle named " + value + ". Using CLOUD as default.");
            particle = Particle.CLOUD;
        }

        getPlayer().getWorld().spawnParticle(particle, getPlayer().getLocation(), count);
    }
}
