package solutions.nuhvel.spigot.advancedafk.tasks;

import org.bukkit.entity.Player;
import solutions.nuhvel.spigot.advancedafk.AdvancedAFK;
import solutions.nuhvel.spigot.advancedafk.utils.AFKHandler;
import solutions.nuhvel.spigot.advancedafk.utils.AFKPlayer;

public abstract class Task implements Runnable {
    private AdvancedAFK plugin;
    private int id;
    private Player p;

    public AdvancedAFK getMain() {
        return plugin;
    }

    public void setMain(AdvancedAFK advancedAFK) {
        this.plugin = advancedAFK;
    }

    public int getID() {
        return id;
    }

    public void setID(int id) {
        this.id = id;
    }

    public Player getPlayer() {
        return p;
    }

    public void setPlayer(Player p) {
        this.p = p;
    }

    public AFKPlayer getAFKPlayer() {
        return AFKHandler.getAFKPlayer(getMain(), getPlayer());
    }

    @Override
    public abstract void run();
}
