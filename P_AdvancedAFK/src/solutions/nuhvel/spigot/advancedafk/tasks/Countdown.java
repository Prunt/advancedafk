package solutions.nuhvel.spigot.advancedafk.tasks;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import solutions.nuhvel.spigot.advancedafk.AdvancedAFK;

import java.util.logging.Level;

public class Countdown extends Task {
    private int i;
    private String t;

    public Countdown(AdvancedAFK plugin, Player p, String type) {
        setMain(plugin);
        setPlayer(p);
        setCount(5);
        setType(type);
    }

    public int getCount() {
        return i;
    }

    public void setCount(int i) {
        this.i = i;
    }

    public void reduceCount() {
        this.i--;
    }

    public String getType() {
        return t;
    }

    public void setType(String t) {
        this.t = t;
    }

    @Override
    public void run() {
        if (AdvancedAFK.DEBUG)
            System.out.println(getPlayer().getName() + " countdown for " + getType() + ": " + getCount());

        // Get valid Sound
        Sound sound = null;
        String name = getMain().getSettings().getString("countdown." + getType() + ".sound");
        try {
            sound = Sound.valueOf(name);
        } catch (IllegalArgumentException e) {
            // Leave sound == null when name == false
            if (!name.equalsIgnoreCase("false")) {
                if (Bukkit.getVersion().contains("1.12")) {
                    sound = Sound.valueOf("BLOCK_NOTE_PLING");
                } else {
                    sound = Sound.valueOf("BLOCK_NOTE_BLOCK_PLING");
                }

                Bukkit.getLogger().log(Level.WARNING,
                        "Couldn't find a Sound named " + name + ". Using " + sound + " as default.");
            }
        }

        // Still countdown
        if (getCount() != 0) {
            String title = getMain().getSettings().getMessage("countdown." + getType() + ".title");
            String subtitle = getMain().getSettings().getMessage("countdown." + getType() + ".subtitle");

            title = title.replaceAll("%count%", String.valueOf(getCount()));
            subtitle = subtitle.replaceAll("%count%", String.valueOf(getCount()));

            getPlayer().sendTitle(title, subtitle, 0, 21, 0);
            if (sound != null)
                getPlayer().playSound(getPlayer().getLocation(), sound, 1, 1);

            reduceCount();
            return;
        }

        // Countdown is over
        String subtitle = getMain().getMessages().getMessage("afk.enabled").replaceAll("%player%",
                getPlayer().getName());

        getPlayer().sendTitle("", subtitle, 0, 40, 20);
        if (sound != null)
            getPlayer().playSound(getPlayer().getLocation(), sound, 1, 1);

        getAFKPlayer().execute(getType());

        // Cancel task
        Bukkit.getScheduler().cancelTask(getID());
    }
}
