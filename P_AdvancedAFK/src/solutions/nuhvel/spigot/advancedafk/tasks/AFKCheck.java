package solutions.nuhvel.spigot.advancedafk.tasks;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import solutions.nuhvel.spigot.advancedafk.AdvancedAFK;
import solutions.nuhvel.spigot.advancedafk.utils.AFKHandler;
import solutions.nuhvel.spigot.advancedafk.utils.AFKPlayer;

public class AFKCheck extends Task {
    public AFKCheck(AdvancedAFK plugin) {
        setMain(plugin);
    }

    @Override
    public void run() {
        for (Player p : Bukkit.getOnlinePlayers()) {
            // World check
            if (getMain().getUtils().isDisabledWorld(p.getWorld().getName()))
                continue;

            // Spectator check
            if (getMain().getSettings().isEnabled("auto.exempt.spectator") && p.getGameMode() == GameMode.SPECTATOR)
                continue;

            AFKPlayer ap = AFKHandler.getAFKPlayer(getMain(), p);

            ap.incrementInactiveSecs();

            if (AdvancedAFK.DEBUG)
                System.out.println("AFKCheck: " + p.getName());

            check(ap, "afk");
            check(ap, "teleport");
            check(ap, "kick");
        }
    }

    private void check(AFKPlayer ap, String type) {
        Player p = ap.getPlayer();

        if (!getMain().getSettings().isEnabled("auto." + type + ".enabled")
                || p.hasPermission("advancedafk.bypass.exempt." + type))
            return;

        if (AdvancedAFK.DEBUG)
            System.out.println(p.getName() + " check for " + type);

        ap.countdown(type);
    }
}
